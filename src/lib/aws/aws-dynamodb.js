import './aws-config';

const KEY_CONFIG = { apiVersion: '2012-10-08', maxRetries: 5 }
export const saveToDatabase = (params, callback) => {
  // Create the DynamoDB service object
  let dynamoDb = new AWS.DynamoDB(KEY_CONFIG);

  let insertExecute = function(callback) {
    dynamoDb.putItem(params, function (err, data){
      callback(err, data);
    })
  }

  insertExecute(callback)
}


export const querySelector = (params, callback) => {
  let dynamoDb = new AWS.DynamoDB(KEY_CONFIG);
  let scanExecute = function(callback) {
    dynamoDb.scan(params, function(err, data) {
      callback(err, data);
    });
  }

  scanExecute(callback);
}

export const deleteItem = (params, callback) => {
  let dynamoDb = new AWS.DynamoDB(KEY_CONFIG);

  let deleteData = function(callback) {
    console.log(params, "=3654047-505-70-")
    dynamoDb.deleteItem(params, function(err, data) {
      callback(err, data);
    });
  }

  deleteData(callback);
}
