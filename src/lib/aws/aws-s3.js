import AWS from './aws-config';

// initiate uploading
export const uploadFileToS3 = (SelectedEventArgs, guid, fileData, index) => {
  // bucket name should copy from
  let bucket = 'cdn.fod247.io/www/thumbnails/LiveClass'
  var upload = new AWS.S3.ManagedUpload({
    partSize: 10 * 1024 * 1024, // default is 5 MB
    queueSize: 10, // Default is 4, the size of the concurrent queue manager to upload parts in parallel
    params: {
      Bucket: bucket,
      Key: fileData.name,
      Body: SelectedEventArgs.filesData[index].rawFile,
      guid: guid
    }
  });

  return upload;
}

export const uploadCustomThumbnail = (fileName, guid, fileData) => {

  // bucket name should copy from
// let bucket = 'content-intake.fod247/' + 'Thumbnail'
let bucket = "test.dev.env";

  console.log("bucket", bucket);
  var upload = new AWS.S3.ManagedUpload({
    partSize: 10 * 1024 * 1024, // default is 5 MB
    queueSize: 10, // Default is 4, the size of the concurrent queue manager to upload parts in parallel
    params: {
      Bucket: bucket,
      Key: fileName,
      Body: fileData,
      guid: guid 
    }
  });

  return upload;
}
