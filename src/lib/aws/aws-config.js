import AWS from 'aws-sdk';

AWS.config.update({
  region: '',
  accessKeyId: '',
  secretAccessKey: ''
});

export default AWS;

