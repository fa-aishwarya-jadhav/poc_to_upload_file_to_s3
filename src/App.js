import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts'

import { uploadCustomThumbnail } from './lib/aws/aws-s3';
class App extends Component {

  state={
//     data : [
//     {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
//     {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
//     {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
//     {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
//     {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
//     {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
//     {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
// ]
data:[{},{},{}]
  }

  uploadThumbnailAndPreview = (event) => {
    let file = event.target.files[0];

    this.uploadContentToS3(file)
  }

  uploadContentToS3(file, type){
    let guid = "123"
    let fileName = `${guid}_${'test_user'}_${file.name}`;
    let that = this;
    uploadCustomThumbnail(fileName, guid, file, "thumbnail").send(function (error, data, t) {
      if (error) {
        console.log("Error ", error)
      } else {
        let attribute = type == 'preview' ? {previewS3Path: data.Location} : {thumbnailS3Path: data.Location};
        that.setState({liveClassThumbnailURL : data.Location});
        that.updateFileObject(guid, attribute, true)
        that.fileSeleteced(that.state.selectedFile)
      }
    });
  }

  render() {
    // return (
    //   <div className="App">
    //     <header className="App-header">
    //       <img src={logo} className="App-logo" alt="logo" />
    //       <p>
    //         Edit <code>src/App.js</code> and save to reload.
    //       </p>
    //       <a
    //         className="App-link"
    //         href="https://reactjs.org"
    //         target="_blank"
    //         rel="noopener noreferrer"
    //       >
    //         Learn React
    //       </a>
    //     </header>
    //   </div>
    // );
    return (
    	// <LineChart width={600} height={300} data={this.state.data}
      //       margin={{top: 5, right: 30, left: 20, bottom: 5}}>
      //  <XAxis dataKey="name"/>
      //  <YAxis/>
      //  <CartesianGrid strokeDasharray="3 3"/>
      //  <Tooltip/>
      //  <Legend />
      //  <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{r: 8}}/>
      //  <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
      // </LineChart>
      <input type="file" name="myfile"  onChange={(e) => this.uploadThumbnailAndPreview(e)} />
    );
  }
}

export default App;
